import {Component, OnInit} from '@angular/core';
import {UploadService} from './upload.service';
import {FileData} from './file-data';

@Component({
  selector: 'file-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  public files: FileData[] = [];
  public selectedFile: File = null;
  public uploadSuccess = false;
  public responseMessage: string = null;
  public selectedUserId = 1;

  constructor(private uploadService: UploadService) {
  }

  ngOnInit() {
    this.getUploadedFiles();
  }

  selectFile($event): void {
    this.selectedFile = $event.target.files[0];
  }

  getUploadedFiles(): void {
    this.uploadService.getFiles(this.selectedUserId).subscribe(data => {
      this.files = data;
    });
  }

  upload(): void {
    this.responseMessage = null;
    this.uploadSuccess = false;
    this.uploadService.uploadFiles(this.selectedUserId, this.selectedFile).subscribe(data => {
      this.uploadSuccess = true;
      this.responseMessage = 'File upload successful with id ' + data;
      this.getUploadedFiles();
    }, error => {
      this.uploadSuccess = false;
      this.responseMessage = error.message;
    });
  }

  shareFile(fileId: number): void {
    this.uploadService.shareFile(this.getToggledUserId(), fileId).subscribe(data => {
      this.uploadSuccess = true;
      this.responseMessage = 'File shared with user with id ' + this.getToggledUserId();
      this.getUploadedFiles();
    }, error => {
      this.uploadSuccess = false;
      this.responseMessage = error.message;
    });
  }

  toggleUserId(): void {
    this.selectedUserId = this.getToggledUserId();
    this.getUploadedFiles();
    this.responseMessage = null;
  }

  getToggledUserId(): number {
    return this.selectedUserId === 1 ? 2 : 1;
  }
}
