export interface FileData {
  id: number;
  fileType: string;
  fileName: string;
  fileUrl: string;
}
