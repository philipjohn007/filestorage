import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FileData} from './file-data';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  constructor(private http: HttpClient) {
  }

  getFiles(userId: number): Observable<FileData[]> {
    return this.http.get<FileData[]>('/api/file/' + userId);
  }

  uploadFiles(userId: number, file: File): Observable<any> {
    const formdata: FormData = new FormData();

    formdata.append('file', file);
    return this.http.post<number>('/api/file/' + userId, formdata);
  }

  shareFile(newUserId: number, fileId: number): Observable<any> {
    return this.http.put<any>('/api/file/' + newUserId + '/' + fileId, null);
  }
}
