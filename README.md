# FileStorage

FileStorage is a Spring boot application for file storage and sharing of files.

## Requirements

The following technologies are required for running the application

```bash
Java 11
Maven
```

## Starting the application

Go to the project folder and execute the following statements

```bash
mvn clean install
java -jar server/target/file-storage-server-0.0.1-SNAPSHOT.jar
```

Open port [http://localhost:8080] for the application.

Swagger docs: [http://localhost:8080/swagger-ui.html]

H2 Console: [http://localhost:8080/h2-console]

## Functionalities

1. Toggle the user between user with id 1 and 2
2. Upload file for each selected user (selected using `Toggle user` button)
3. Display the uploaded files of the selected users
4. Download the uploaded files
5. Share the uploaded file to the other user (shared file comes in the list of files for the other user)