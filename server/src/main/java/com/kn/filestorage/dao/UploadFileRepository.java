package com.kn.filestorage.dao;

import com.kn.filestorage.domain.UploadFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {

    Stream<UploadFile> findDistinctByAccessibleUsersIdOrderById(Long userId);

    Optional<UploadFile> findByIdAndAccessibleUsersId(Long id, Long userId);
}
