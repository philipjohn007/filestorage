package com.kn.filestorage.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UploadFileDto {

    @NotNull
    private Long id;

    @NotNull
    private String fileName;

    @NotNull
    private String fileType;

    @NotNull
    private String fileUrl;

    @NotNull
    private byte[] content;
}
