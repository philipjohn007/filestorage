package com.kn.filestorage.mapper;

import com.kn.filestorage.domain.UploadFile;
import com.kn.filestorage.dto.UploadFileDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Mapper(componentModel = "spring")
public abstract class UploadFileMapper {

    @Mapping(target = "fileUrl", source = "uploadFile")
    public abstract UploadFileDto uploadFileToUploadFileDto(UploadFile uploadFile);

    String uploadFileUrl(UploadFile uploadFile) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file/" + uploadFile.getAccessibleUsers().iterator().next().getId() + "/")
                .path(uploadFile.getId().toString())
                .toUriString();
    }
}
