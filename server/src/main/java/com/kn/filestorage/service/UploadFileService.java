package com.kn.filestorage.service;

import com.kn.filestorage.dao.ApplicationUserRepository;
import com.kn.filestorage.dao.UploadFileRepository;
import com.kn.filestorage.domain.UploadFile;
import com.kn.filestorage.dto.UploadFileDto;
import com.kn.filestorage.exception.FileNotFoundException;
import com.kn.filestorage.exception.FileUploadException;
import com.kn.filestorage.mapper.UploadFileMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class UploadFileService {

    private final UploadFileRepository uploadFileRepository;
    private final UploadFileMapper uploadFileMapper;
    private final ApplicationUserRepository applicationUserRepository;

    @Autowired
    public UploadFileService(
            UploadFileRepository uploadFileRepository,
            UploadFileMapper uploadFileMapper,
            ApplicationUserRepository applicationUserRepository) {
        this.uploadFileRepository = uploadFileRepository;
        this.uploadFileMapper = uploadFileMapper;
        this.applicationUserRepository = applicationUserRepository;
    }

    public List<UploadFileDto> getAllUploadedFiles(Long userId) {
        return uploadFileRepository.findDistinctByAccessibleUsersIdOrderById(userId)
                .map(uploadFileMapper::uploadFileToUploadFileDto)
                .collect(Collectors.toList());
    }

    public Long uploadFile(Long userId, MultipartFile multipartFile) {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        try {
            UploadFile uploadFile = UploadFile.builder()
                    .fileName(fileName).fileType(multipartFile.getContentType()).content(multipartFile.getBytes())
                    .accessibleUsers(Set.of(applicationUserRepository.getOne(userId)))
                    .build();

            return uploadFileRepository.save(uploadFile).getId();
        } catch (IOException e) {
            log.error("Error uploading file " + fileName);
            throw new FileUploadException("Error upload files", e);
        }
    }

    public UploadFileDto getUploadedFileById(Long userId, Long uploadFileId) {
        UploadFile uploadFile = uploadFileRepository.findByIdAndAccessibleUsersId(uploadFileId, userId)
                .orElseThrow(() -> new FileNotFoundException("File with id " + uploadFileId + " is not found"));

        return uploadFileMapper.uploadFileToUploadFileDto(uploadFile);
    }

    public Long shareFile(Long uploadFileId, Long newUserId) {
        UploadFile uploadFile = uploadFileRepository.findById(uploadFileId)
                .orElseThrow(() -> new FileNotFoundException("File with id " + uploadFileId + " is not found"));

        uploadFile.setAccessibleUsers(new HashSet<>(uploadFile.getAccessibleUsers()));
        uploadFile.getAccessibleUsers().add(applicationUserRepository.getOne(newUserId));

        return uploadFileRepository.save(uploadFile).getId();
    }
}
