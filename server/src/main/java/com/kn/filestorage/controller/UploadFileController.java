package com.kn.filestorage.controller;

import com.kn.filestorage.dto.UploadFileDto;
import com.kn.filestorage.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/file")
public class UploadFileController {

    private final UploadFileService uploadFileService;

    @Autowired
    public UploadFileController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<UploadFileDto>> getAllUploadFiles(@PathVariable Long userId) {
        return ResponseEntity.ok(uploadFileService.getAllUploadedFiles(userId));
    }

    @PostMapping("/{userId}")
    public ResponseEntity<Long> uploadFile(@PathVariable Long userId, @RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(uploadFileService.uploadFile(userId, file));
    }

    @GetMapping("/{userId}/{fileId}")
    public ResponseEntity<Resource> getFile(@PathVariable Long userId, @PathVariable Long fileId) {
        UploadFileDto uploadFileDto = uploadFileService.getUploadedFileById(userId, fileId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(uploadFileDto.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + uploadFileDto.getFileName() + "\"")
                .body(new ByteArrayResource(uploadFileDto.getContent()));
    }

    @PutMapping("/{newUserId}/{fileId}")
    public ResponseEntity<Long> shareFile(@PathVariable Long newUserId, @PathVariable Long fileId) {
        return ResponseEntity.ok(uploadFileService.shareFile(fileId, newUserId));
    }
}
