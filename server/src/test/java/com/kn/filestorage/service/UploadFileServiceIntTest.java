package com.kn.filestorage.service;

import com.kn.filestorage.dao.UploadFileRepository;
import com.kn.filestorage.domain.ApplicationUser;
import com.kn.filestorage.dto.UploadFileDto;
import com.kn.filestorage.exception.FileNotFoundException;
import com.kn.filestorage.helper.UploadFileDtoFixture;
import com.kn.filestorage.mapper.UploadFileMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.kn.filestorage.constant.TestConstants.DEFAULT_ID;
import static com.kn.filestorage.constant.TestConstants.INVALID_ID;
import static com.kn.filestorage.constant.TestConstants.SECOND_USER_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
@Transactional
class UploadFileServiceIntTest {

    @Autowired
    private UploadFileService uploadFileService;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private UploadFileMapper uploadFileMapper;

    private UploadFileDto uploadFileDto;
    private Long uploadFileId;

    @BeforeEach
    void setup() {
        uploadFileDto = UploadFileDtoFixture.buildUploadFileDto();
        MockMultipartFile file = new MockMultipartFile(
                "file",
                uploadFileDto.getFileName(),
                uploadFileDto.getFileType(),
                uploadFileDto.getContent());
        uploadFileId = uploadFileService.uploadFile(DEFAULT_ID, file);
    }

    @Test
    void shouldGetAllUploadedFilesSuccessfully() {
        List<UploadFileDto> uploadFileDtos = uploadFileService.getAllUploadedFiles(DEFAULT_ID);

        assertThat(uploadFileDtos).isNotEmpty();
        assertThat(uploadFileDtos).extracting(UploadFileDto::getId).contains(uploadFileId);
    }

    @Test
    void shouldUploadFileSuccessfully() {
        UploadFileDto uploadFileDto = UploadFileDtoFixture.buildUploadFileDto();
        MockMultipartFile file = new MockMultipartFile(
                "file",
                uploadFileDto.getFileName(),
                uploadFileDto.getFileType(),
                uploadFileDto.getContent());
        Long uploadFileId = uploadFileService.uploadFile(DEFAULT_ID, file);

        UploadFileDto foundDto = uploadFileMapper.uploadFileToUploadFileDto(uploadFileRepository.getOne(uploadFileId));

        assertThat(foundDto).isNotNull();
        assertThat(foundDto).isEqualToIgnoringGivenFields(uploadFileDto, "id", "fileUrl");
    }

    @Test
    void shouldGetUploadedFileByIdSuccessfully() {
        UploadFileDto foundDto = uploadFileService.getUploadedFileById(DEFAULT_ID, uploadFileId);

        assertThat(foundDto).isEqualToIgnoringGivenFields(uploadFileDto, "id", "fileUrl");
        assertThat(foundDto.getFileUrl()).contains("/api/file/"+ DEFAULT_ID + "/" + uploadFileId);
    }

    @Test
    void shouldThrowExceptionWhenGetUploadedFileByIdWithInvalidId() {
        assertThatThrownBy(() -> uploadFileService.getUploadedFileById(DEFAULT_ID, INVALID_ID)).isInstanceOf(FileNotFoundException.class)
                .hasMessage("File with id " + INVALID_ID + " is not found");
    }

    @Test
    void shouldShareFileSuccessfully() {
        assertThat(uploadFileRepository.getOne(uploadFileId).getAccessibleUsers()).extracting(ApplicationUser::getId)
                .contains(DEFAULT_ID).doesNotContain(SECOND_USER_ID);

        uploadFileService.shareFile(uploadFileId, SECOND_USER_ID);

        assertThat(uploadFileRepository.getOne(uploadFileId).getAccessibleUsers()).extracting(ApplicationUser::getId)
                .contains(DEFAULT_ID, SECOND_USER_ID);
    }

    @Test
    void shouldThrowExceptionWhenShareFileWithInvalidId() {
        assertThatThrownBy(() -> uploadFileService.shareFile(INVALID_ID, SECOND_USER_ID)).isInstanceOf(FileNotFoundException.class)
                .hasMessage("File with id " + INVALID_ID + " is not found");
    }
}
