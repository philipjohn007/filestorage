package com.kn.filestorage.helper;

import com.kn.filestorage.dto.UploadFileDto;
import lombok.experimental.UtilityClass;

import static com.kn.filestorage.constant.TestConstants.FILE_CONTENT;
import static com.kn.filestorage.constant.TestConstants.FILE_NAME;
import static com.kn.filestorage.constant.TestConstants.FILE_TYPE;
import static com.kn.filestorage.constant.TestConstants.FILE_URL;

@UtilityClass
public final class UploadFileDtoFixture {

    public static UploadFileDto buildUploadFileDto() {
        return UploadFileDto.builder()
                .fileName(FILE_NAME).fileType(FILE_TYPE).content(FILE_CONTENT).fileUrl(FILE_URL)
                .build();
    }
}
