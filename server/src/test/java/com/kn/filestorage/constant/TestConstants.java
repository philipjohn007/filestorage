package com.kn.filestorage.constant;

import lombok.experimental.UtilityClass;
import org.springframework.http.MediaType;

@UtilityClass
public final class TestConstants {

    public static final Long DEFAULT_ID = 1L;
    public static final Long SECOND_USER_ID = 2L;
    public static final Long INVALID_ID = 1024L;
    public static final String FILE_NAME = "File name.txt";
    public static final String FILE_TYPE = MediaType.TEXT_PLAIN_VALUE;
    public static final byte[] FILE_CONTENT = "File content".getBytes();
    public static final String FILE_URL = "http://localhost:8080/1";
}
