package com.kn.filestorage.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kn.filestorage.dto.UploadFileDto;
import com.kn.filestorage.helper.UploadFileDtoFixture;
import com.kn.filestorage.service.UploadFileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.kn.filestorage.constant.TestConstants.DEFAULT_ID;
import static com.kn.filestorage.constant.TestConstants.FILE_CONTENT;
import static com.kn.filestorage.constant.TestConstants.FILE_NAME;
import static com.kn.filestorage.constant.TestConstants.FILE_TYPE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class UploadFileControllerTest {

    private static final String API_FILE_UPLOAD = "/api/file/" + DEFAULT_ID;
    private static final String API_FILE_UPLOAD_BY_ID = "/api/file/" + DEFAULT_ID + "/" + DEFAULT_ID;

    private MockMvc mvc;

    @MockBean
    private UploadFileService uploadFileService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper objectMapper;

    private UploadFileDto uploadFileDto = UploadFileDtoFixture.buildUploadFileDto();

    @BeforeEach
    void setupMocks() {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void shouldUploadFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", FILE_NAME, FILE_TYPE, FILE_CONTENT);
        when(uploadFileService.uploadFile(anyLong(), any(MultipartFile.class))).thenReturn(DEFAULT_ID);

        mvc.perform(multipart(API_FILE_UPLOAD).file(file))
                .andExpect(status().isOk())
                .andExpect(content().string(DEFAULT_ID.toString()));
    }

    @Test
    void shouldGetUploadedFiles() throws Exception {
        List<UploadFileDto> uploadFileDtos = List.of(uploadFileDto);
        when(uploadFileService.getAllUploadedFiles(anyLong())).thenReturn(uploadFileDtos);

        mvc.perform(get(API_FILE_UPLOAD))
                .andExpect(content().string(objectMapper.writeValueAsString(uploadFileDtos)))
                .andExpect(status().isOk());
    }

    @Test
    void shouldGetUploadedFileById() throws Exception {
        when(uploadFileService.getUploadedFileById(DEFAULT_ID, DEFAULT_ID)).thenReturn(uploadFileDto);

        mvc.perform(get(API_FILE_UPLOAD_BY_ID))
                .andExpect(content().bytes(uploadFileDto.getContent()))
                .andExpect(status().isOk());
    }

    @Test
    void shouldShareUploadedFile() throws Exception {
        when(uploadFileService.shareFile(DEFAULT_ID, DEFAULT_ID)).thenReturn(DEFAULT_ID);

        mvc.perform(put(API_FILE_UPLOAD_BY_ID))
                .andExpect(content().string(DEFAULT_ID.toString()))
                .andExpect(status().isOk());
    }
}
